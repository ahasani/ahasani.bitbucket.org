/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var topics = {};

jQuery.Topic = function( id ) {
  var callbacks,
  method,
  topic = id && topics[ id ];
  if ( !topic ) {
    callbacks = jQuery.Callbacks();
    topic = {
      publish: callbacks.fire,
      subscribe: callbacks.add,
      unsubscribe: callbacks.remove
    };
    if ( id ) {
      topics[ id ] = topic;
    }
  }
  return topic;
};

jQuery.isDate = function(obj){
  return(obj.constructor===Date);
}

var Request = {
  parameter: function(name) {
    return this.parameters()[name];
  },

  parameters: function() {
    var result = {};
    var url = window.location.href;
    var parameters = url.slice(url.indexOf('?') + 1).split('&');

    for(var i = 0;  i < parameters.length; i++) {
      var parameter = parameters[i].split('=');
      result[parameter[0]] = parameter[1];
    }
    return result;
  }
}

var numberFormat = {
  symbol:'Rp. ',
  roundToDecimalPlace:0,
  digitGroupSymbol:'.'
}

var modalOptions = {
  backdrop:true,
  keyboard:true
}

function isDate(formId,elementId){
  var element = $('#'+formId+' input[name="'+elementId+'"]');
  element.datepicker();
  element.attr("name-type","date");
}

function isDateTime(formId,elementId){
  var element = $('#'+formId+' input[name="'+elementId+'"]');
  element.datetimepicker();
  element.attr("name-type","datetime");
}

function isNumber(formId,elementId){
  var element = $('#'+formId+' input[name="'+elementId+'"]');
  element.attr("name-type","number");
}

function MapToInputElement(obj,formId){
  $.each(obj, function(key, value) {
    var element = $('#'+formId +' [name="'+key+'"]');
    if (element.attr("name-type")=="date"){
      var dateValue = new Date(value.$date);
      element.datepicker("setDate",dateValue);
    }
    else if (element.attr("name-type")=="datetime"){
      var dateTimeValue = new Date(value.$date);
      element.datetimepicker("setDate",dateTimeValue);
    }
    else if (element.attr("type")=="checkbox"){
      element[0].checked = value;
    }
    else{
      element.val(value);
    }
  });
}

function MapFromInputElement(formId){
  var object = {};
  var formElements = $('#'+formId + ' [name]');
  formElements.each(function(){
    var element = $(this);
    if (element.attr("type")=="checkbox"){
      object[element.attr("name")]=element.context.checked;
    }
    else if (element.attr("name-type")=="number"){
      if (element.val()!=""){
        object[element.attr("name")]=parseFloat(element.val());
      }
    }
    else if (element.attr("name-type")=="date"){
      if (element.datepicker("getDate")!=null){
        var dateString = new Date(element.datepicker("getDate"));
        object[element.attr("name")]={
          $date:element.datepicker("getDate")
          //,value:dateString.toDateString() +" "+ dateString.toTimeString()
        };
        //        object[element.attr("name")]=element.datepicker("getDate").getTime();
      }
      else {
        object[element.attr("name")]=element.val();
      }
    }
    else if (element.attr("name-type")=="datetime"){
      var dateString = new Date(element.datepicker("getDate"));
      if (element.datepicker("getDate")!=null){
        object[element.attr("name")]={
          $date:element.datetimepicker("getDate")
          //,value:dateString.toDateString() +" "+ dateString.toTimeString()
        };
        //        object[element.attr("name")]=element.datepicker("getDate").getTime();
      }
      else {
        object[element.attr("name")]=element.val();
      }
    }
    else  {
      object[element.attr("name")]=element.val();
    }
  });
  return object;
}

function formToDataModel(form,dataModel){
  var obj = MapFromInputElement(form);
  //console.log(obj);
  $.each(obj,function(index,value){
    dataModel[index] = value;
  })
}

function ToggleShow(element,panel){
  if($(element).text()=="Hide"){
    $("#"+panel).hide(300);
    $(element).text("Show");
  }
  else {
    $("#"+panel).show(300);
    $(element).text("Hide");
  }
}

function showAlert(type,msg){
  var element = $('<div id="alert"class="alert-message hide" style="position:fixed;top:40px;width:98%;z-index:999;right:0px">test</div>');
  element.addClass(type);
  element.text(msg);
  element.appendTo(document.body);
  element.show(300).delay(1000).hide(300,function(){
    $(this).remove();
  });
}

function GetSequence(path){
  var id = "";
  $.ajax({
    url: '/data/sequence?name='+path,
    type: "get",
    async: false,
    dataType:'json',
    success: function(obj){
      id = obj.value
    }
  });
  return id;
}

function CheckValueExist(path,field,value){
  var exist = false;
  $.ajax({
    url: path,
    data: '{"'+field+'":"'+value+'"}',
    type: "get",
    async: false,
    dataType:'json',
    success: function(obj){
      if(typeof obj.data != "undefined")
        if(obj.data.length > 0){
          exist = true;
        }
    }
  });
  return exist;
}

function loadContent(url,container){
  $.ajax({
    url:url,
    dataType:'html',
    type:'get',
    success:function(obj){
      $("#"+container).html(obj);
    }
  });
}

function getDateTime(date){
  var dateTime = {};
  var dateObj = new Date(date);
  var month = dateObj.getMonth() + 1;
  dateTime.date = ""+dateObj.getDate()+"-"+month+"-"+dateObj.getFullYear();
  dateTime.time = ""+dateObj.getHours()+":"+dateObj.getMinutes()+":"+dateObj.getSeconds();
  return dateTime;
}

function loadTemplate(url,data,fn){
  $.ajax({
    url:url,
    dataType:"html",
    success: function(obj){
      var html = Mustache.to_html(obj, data);
      fn(html);
    }
  });
}

  function showDialog(header,body,url){
    var modalBody =  '\n\
          <div id="modalDialog" class="modal hide fade in">\n\
          <div class="modal-header">\n\
            <a href="#" class="close">×</a>\n\
            <h3><div id="modalHeader"></div></h3>\n\
          </div>\n\
          <div class="modal-body" id="modalBody" style="height:300px;overflow:auto"></div>\n\
          </div>\n\
        ';
    $(document.body).append(modalBody);
    $('#modalDialog').modal(modalOptions);
    if (url==undefined){
      $("#modalBody").html(body);
      $("#modalHeader").text(header);
    }
    else{
      $.ajax({
        url:url,
        dataType:'html',
        type:'get',
        success:function(obj){
          $("#modalDialog").html(obj);
        }
      });
    }
    $('#modalDialog').bind('hidden', function () {
      $('#modalDialog').remove();
    });
    $('#modalDialog').modal('show');
  }
